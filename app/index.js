var columns = [
    {
        title: 'Local Dev Sites',
        links: [
            {
                title: 'HTSAB',
                url: 'http://www.howtostartabusiness.test.172.17.2.192.xip.io:8888'
            },
            {
                title: 'Delaware',
                url: 'http://www.delawareregisteredagent.test.172.17.2.192.xip.io:8888'
            },
            {
                title: 'NorthWest',
                url: 'http://northwestregisteredagent.test.172.17.2.192.xip.io:8888'
            }
        ]
    },
    {
        title: 'Live Sites',
        links: [
            {
                title: 'HTSAB',
                url: 'https://howtostartabusiness.org'
            },
            {
                title: 'Delaware',
                url: 'https://delawareregisteredagent.com'
            },
            {
                title: 'NorthWest',
                url: 'https://northwestregisteredagent.com'
            }
        ]
    },
    {
        title: 'Live WP Admin',
        links: [
            {
                title: 'HTSAB',
                url: 'https://howtostartabusiness.org/wp-admin'
            },
            {
                title: 'Delaware',
                url: 'https://delawareregisteredagent.com/wp-admin'
            },
            {
                title: 'NorthWest',
                url: 'https://northwestregisteredagent.com/wp-admin'
            }
        ]
    }
];


function render_columns() {
    /*
    chrome.storage.sync.get({}, function(options) {
        if (options.columns) {
            console.log(options.columns);
        }
    });

     */
    $.each(columns, function(i, val) {
        var col;
        console.log(val);
        val.colWidth = 100 / columns.length;
        col = $(fill_template('column', val));
        if (val.links && val.links.length) {
            $.each(val.links, function(i, link) {
                col.find('.link-container').append($(fill_template('row', link)));
            });
        }
        $('main > .container > .row').append(col);
    });
}

function get_template(name) {
    var el = $('script[data-template="' + name + '"]');
    return el.length? el.text() : null;
}

function fill_template(name, data) {
    function render(props) {
        return function(tok, i) {
            return (i % 2) ? props[tok] : tok;
        };
    }
    if (typeof data === 'object') {
        data = [data];
    }
    var templateContents = get_template(name);
    if (!templateContents) {
        return null;
    }
    var token = templateContents.split(/\$\{(.+?)\}/g);
    return data.map(function (item) {
        return token.map(render(item)).join('');
    })[0];
}


document.addEventListener('DOMContentLoaded', render_columns);