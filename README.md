# Custom New Tab Page Extension

## Installation

1. Clone the repo
2. Go to the chrome extensions page
3. In top right corner enable developer mode
4. In the newly appeared menu, click "Load Unpacked"
5. Locate the `app` folder in the project files and select it
6. Done!

## Usage

* Open a new empty tab to use
* For now the tabs and links have to be defined in `index.js`
* If a modification is made to the source files, the extension has to be reloaded in the chrome extensions page

## TODO

* Finish the options page to allow easy set up of columns and links
* Process links to force absolute URL's
* Package the extension